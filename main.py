#coding=utf-8
"""
main.py - CATS main file. mew

Documentation: mew mew mew mew mew mew

Copyright (c) 2015 Weloxux and The Capitalist Crayfish

Credits:
- Programming, explosion sound effect: Marnix "Weloxux" Massar
- Sprites: Brzoz
- Sound: Daniil "daniilS" Soloviev
- A ton of ideas: Ness

- NES soundfont: THE EIGHTH BIT
- Spritesheet library: scriptefun.com, according to the Pygame wiki
"""
#Please note: this code was made in a big hurry, and therefore is a giant mess. I know the mobs should have used classes,
#I know that those sprites could probably have been loaded in a single line of code, I know I waste a lot of the cycles checking
#for stuff that is not going to be there. But hey, everything seems to work, so don't complain and play.
# -Weloxux

import sys #Fur exiting and appawding paths, so we can inpurrt stuff not in our Python lib
import pygame #Speaks fur itself. mew
from pygame.locals import * #Because typing is stupid
import spritesheet #Fur interpreting spritesheets, of course
import random #Fur random spawning

pygame.init() #Initialize pygame
pygame.font.init() #Initialize fonts. pygame.init() should do the trick, but hey, one cannot be too sure

window = pygame.display.set_mode((952, 680),0,32) #Start up a window. Tiles will be 136x136
pygame.display.set_caption("CATS - kitty-Cat Arnold's non-Typical Stroll")
pygame.FULLSCREEN = True

pygame.mixer.init(frequency=44100, size=8, channels=3, buffer=2048)
initiated = pygame.mixer.Sound("sound/music/initiated.wav") #Only plays the first time
inane = pygame.mixer.Sound("sound/music/inane.wav") #Default
inverse = pygame.mixer.Sound("sound/music/inverse.wav") #For inverse
indurated = pygame.mixer.Sound("sound/music/indurated.wav") #Boss

inanimate = pygame.mixer.Sound("sound/effect/inanimate.wav") #Dying sound
incandescent = pygame.mixer.Sound("sound/effect/incandescent.wav") #Laser shoot
ingested = pygame.mixer.Sound("sound/effect/ingested.wav") #Swallow
ineffective = pygame.mixer.Sound("sound/effect/ineffective.wav") #Laser bounce
internecine = pygame.mixer.Sound("sound/effect/internecine.wav") #Snick snack
inspace = pygame.mixer.Sound("sound/effect/inspace.wav") #Throw stuff
implosion = pygame.mixer.Sound("sound/effect/implosion.wav") #Boss dying


music = pygame.mixer.Channel(1)
effect = pygame.mixer.Channel(2)
effect.set_volume(0.4)
effect2 = pygame.mixer.Channel(3)
effect2.set_volume(0.4)

clock = pygame.time.Clock()
FPS = 10
interval = 33
frames = 0
playing = True

hardmode = "locked"

fishlist, doglist, demonlist, craylist = [], [], [], [] #Gotta do these globally, sadly. Well, there probably is a way, but I am a shit programmer

fontpath = pygame.font.match_font("monospace") #Look for the way to monospace (not Rome)
bigtext = pygame.font.Font(fontpath, 190)
mediumtext = pygame.font.Font(fontpath, 50)
smalltext = pygame.font.Font(fontpath, 50)
tinytext = pygame.font.Font(fontpath, 30)

title = bigtext.render("CATS", 1, (255,255,255))
bosstext = mediumtext.render("A MEW CHEWLENGER APPURRED!",1,(255,255,255))
wontext = mediumtext.render("HARDMODE UNLOCKED",1,(255,255,255))
controls = mediumtext.render("CONTROLS", 1, (255,255,255))
chewtext = smalltext.render("S | CHEW", 1, (255,255,255))
shoottext = smalltext.render("D | SHOOT", 1, (255,255,255))
gameover = mediumtext.render("PRESS S TO RETURN TO THE MENU", 1, (255,255,255))

def menu(mode):
  global clock, FPS, interval, frames, dcatsprites, dfishsprites, ddogsprites, dchews, dopencat, doglist, fishlist
  catsprite, ncatsprite = cat1, 1
  
  if mode == "hardmode":
    modeselect = mediumtext.render("S FOR NORMAL MODE", 1, (255,255,255))
    modeselect2 = mediumtext.render("D FOR HARDMODE", 1, (255,255,255))
  else:
    modeselect = mediumtext.render("PRESS S TO START", 1, (255,255,255))
    modeselect2 = False
  
  fishspeed, dogspeed = 1.4, 1.2 #dogspeed is not used by dogs at all, but by the ground. logic is for nerds, and im 2 cool 4 dat
  catmode, eatstate, laserstate, dying, dead = "default", 0, 0, 0, 0
  
  catheadlocs, groundlocs, grasslocs = {1: (160,400), 2: (160,408), 3: (160,416), 4: (160,408)}, [[0,544], [136,544], [272,544], [408,544], [544,544], [680,544], [816,544], [952, 544]],     [[0,424], [136,424], [272,424], [408,424], [544,424], [680,424], [816,424], [952,424]]
  cathead = chewcat2
  difvar = (136/fishspeed)
  fallings, lasers, splodes, splodesprites, lastsplode = [], [], {}, {}, 0
  liftup = False
  mode,initiate,first = "softmode", False, False
  menuloop = True
  while menuloop == True:
    #PROCESSES
    
    for event in pygame.event.get(): #To process events
      if event.type == QUIT: #Stops the entire program when closed
        pygame.quit()
        sys.exit()
      
      elif event.type == KEYUP:
        if event.key == K_s:
          frames = 0
          game("softmode")
        elif event.key == K_d:
          if modeselect2:
            game("hardmode")
    
    #/PROCESSES
    #LOGIC
    
    frames += 1
    
    if frames % interval == 0: #So like, animation events are here. And stuff
      if ncatsprite == 4:
        ncatsprite = 0
      ncatsprite += 1
      catsprite = dcatsprites[ncatsprite]
      if catmode == "default":
        cathead = chewcat2
    
    catheadloc = catheadlocs[ncatsprite]
    
    for item in groundlocs:
      if not dead and not dying:
        item[0] -= dogspeed
        if item[0] <= -136:
          groundlocs.remove(item)

    for item in grasslocs:
      if not dead and not dying:
        item[0] -= dogspeed
        if item[0] <= -136:
          grasslocs.remove(item)
    
    if music: #Check if the mixer is running
      if pygame.mixer.get_busy() == False: #If nothing is playing yet, we will play initiated
        music.play(initiated)
        music.queue(inane)
      elif music.get_queue() == None:
        music.queue(inane)

    if frames % int(difvar) == 0:
      if not dead and not dying:
        groundlocs.append([952, 544])
        grasslocs.append([952,424])
    
    #/LOGIC
    #DRAW
    window.fill((100,170,220))
    
    for thing in groundlocs:
      window.blit(ground, thing) #Draw the ground
    
    for item in grasslocs:
      window.blit(grass, item) #Draw the grass
    
    window.blit(catsprite, (136,408)) #Draw the body of the cat
    window.blit(cathead, catheadloc) #Then draw the head
    
    window.blit(title, (2,2))
    #if hardmode == "unlocked":
      #window.blit(modeselect, (2,194))
      #indow.blit(tutorial, (2,324))
    #else:
     # window.blit(tutorial, (2,194))
    window.blit(modeselect, (2,194))
    if modeselect2:
      window.blit(modeselect2, (2,244))
      
    window.blit(controls, (670,2))
    window.blit(chewtext, (670, 50))
    window.blit(shoottext, (670, 100))
      
    pygame.display.flip()
    
    #/DRAW


def game(mode): #The game itself
  global clock, FPS, interval, frames, dcatsprites, dfishsprites, ddogsprites, dchews, dopencat, doglist, fishlist, demonlist, craylist
  catsprite, ncatsprite, fishsprite, nfishsprite, dogsprite, ndogsprite = cat1, 1, fish1, 1, dog1, 3
  
  spawninterval, difficulty, fishspeed, dogspeed = 270, 2000, 1.9, 1.2 #dogspeed is not used by dogs at all, but by the ground. logic is for nerds, and im 2 cool 4 dat
  catmode, eatstate, laserstate, dying, dead = "default", 0, 0, 0, 0
  
  if mode == "hardmode":
    fishspeed += 0.3
    spawninterval -= 20
  
  catheadlocs, groundlocs, grasslocs = {1: (160,400), 2: (160,408), 3: (160,416), 4: (160,408)}, [[0,544], [136,544], [272,544], [408,544], [544,544], [680,544], [816,544], [952, 544]],     [[0,424], [136,424], [272,424], [408,424], [544,424], [680,424], [816,424], [952,424]]
  cathead = chewcat2
  difvar = (136/fishspeed)
  fallings, lasers, splodes, splodesprites, lastsplode, backfire = [], [], {}, {}, 0, []
  liftup,deadtext,initiate,first,challenger = False,False,False,False,False
  won = False
  score, score_fish, score_dog, score_boss, mscore_fish = 0,0,0,0,0
  bossstate,shield1s,shield2s,shield3s,tail = 0,False,False,False,6
  hunger = 4
  downlist, attacklist, cooklist, explosions = [], [], [], []
  legtimer,legtimer2 = 1, 3
  laserremove = [] #Bugfix stuff
  while playing == True: #Main loop
    #PROCESSES
    
    for event in pygame.event.get(): #To process events
      if event.type == QUIT: #Stops the entire program when closed
        pygame.quit()
        sys.exit()
      
      elif event.type == KEYDOWN:
        if event.key == K_s:
          catmode = "munch"
        elif event.key == K_d:
          catmode = "laser"
      
      elif event.type == KEYUP:
        if event.key in (K_s, K_d):
          liftup = True
        if dead:
          if event.key == K_s:
            frames = 0
            fishlist,doglist,demonlist,craylist = [],[],[],[]
            menu("softmode")
        elif won:
          if event.key == K_s:
            frames = 0
            fishlist,doglist,demonlist,craylist = [],[],[],[]
            menu("hardmode")
    
    #/PROCESSES
    #LOGIC
    frames += 1
    if not dead or dying:
      scoreframes = frames
    
    if liftup == True:
      if eatstate == 0 and laserstate == 0:
        catmode = "default"
        liftup = False
    
    if frames % interval == 0: #So like, animation events are here. And stuff
      if ncatsprite == 4:
        ncatsprite = 0
      if ndogsprite == 6:
        ndogsprite = 0
      if nfishsprite == 4:
        nfishsprite = 0
      if legtimer == 3:
        legtimer = 0
      if legtimer2 == 3:
        legtimer2 = 0
      ncatsprite += 1
      ndogsprite += 1
      nfishsprite += 1
      legtimer += 1
      legtimer2 += 1
      catsprite = dcatsprites[ncatsprite]
      if catmode == "default":
        cathead = chewcat2
      elif catmode == "munch": #Here is a piece of shit code. Good luck with figuring it out. I really need to learn this programming stuff, yknow
        if eatstate == 0:
          cathead = dopencat[ncatsprite]
        elif eatstate == 1:
          cathead = dchews[ncatsprite]
          if ncatsprite == 2 or ncatsprite == 4:
            eatstate = 3
        elif eatstate == 3:
          cathead = spit1
          eatstate = 0
          fallings.append([160,458])
        elif eatstate == 5:
          cathead = nomcat1
          eatstate = 6
        elif eatstate == 6:
          cathead = nomcat2
          eatstate = 1

    if frames % 14 == 0: #The laser animation is faster than the others
      if catmode == "laser":
        if not dead or dying or eatstate:
          if laserstate == 5:
            lasers.append([216,450])
            laserstate = 0
            catmode = "default"
          elif laserstate == 1:
            effect.queue(incandescent)
            laserstate += 1
            cathead = dlasercat[laserstate]
          else:
            laserstate += 1
            cathead = dlasercat[laserstate]
    
    if frames % 1400 == 0:
      if hunger != 0:
        hunger -= 1
      elif not dying and not dead:
        dying = True
    
    hungertext = tinytext.render("BELLY: " + str(hunger),1,(255,255,255))
    
    for zap in lasers:
      zap[0] += 4
      if zap[0] > 952: #If the laser isn't in the viewport anymore
        lasers.remove(zap)
      for dog in doglist:
        if zap[0] > (dog[0] - 50) and zap[0] < (dog[0] + 136):
          doglist.remove(dog)
          score_dog += 1
          lasers.remove(zap)
          lastsplode += 1
          splodes[lastsplode] = [zap[0], zap[1] - 32]
          splodesprites[lastsplode] = 1
      for fish in fishlist:
        if zap[0] > (fish[0] - 10) and zap[0] < (fish[0] + 136):
          fishlist.remove(fish)
          mscore_fish += 1
          lasers.remove(zap)
          lastsplode += 1
          splodes[lastsplode] = [zap[0], zap[1] - 32]
          splodesprites[lastsplode] = 1
      for cray in craylist:
        if zap[0] > (cray[0] - 10) and zap[0] < (cray[0] + 136):
          craylist.remove(cray)
          cooklist.append(cray)
          lasers.remove(zap)
          score_fish += 1
          lastsplode += 1
          splodes[lastsplode] = [zap[0], zap[1] - 32]
          splodesprites[lastsplode] = 1
      for demon in attacklist:
        if zap[0] > (demon[0] - 20) and zap[0] < (demon[0] + 244):
          attacklist.remove(demon)
          score_dog += 2
          lasers.remove(zap)
          lastsplode += 1
          splodes[lastsplode] = [zap[0], zap[1] - 32]
          splodesprites[lastsplode] = 1
      for demon in downlist:
        if zap[0] > (demon[0] - 20) and zap[0] < (demon[0] + 244) and demon[1] > 320:
          downlist.remove(demon)
          score_dog += 2
          lasers.remove(zap)
          lastsplode += 1
          splodes[lastsplode] = [zap[0], zap[1] - 32]
          splodesprites[lastsplode] = 1
      if bossstate == "stationary":
        if shield3s:
          for shield in ((630, 272),(570, 272),(498, 272)):
            if zap[0] >= shield[0]:
              laserremove.append(zap)
              backfire.append(zap)
              shield3s = False
              effect2.play(ineffective)
        elif shield2s:
          for shield in ((630, 272),(570, 272)):
            if zap[0] >= shield[0]:
              backfire.append(zap)
              lasers.remove(zap)
              shield2s = False
              effect2.queue(ineffective)
        elif shield1s:
          if zap[0] >= 630:
            backfire.append(zap)
            lasers.remove(zap)
            shield1s = False
            bossstate = "flee"
            bosscoords = [510,288]
            spawninterval = 491
            effect2.queue(ineffective)
      elif bossstate == "flee":
        if craylist == [] and cooklist == [] and zap[0] >= 510 + ((6-tail) * 52) and tail != 0:
          explosions.append([(510 + ((6-tail) * 52)), 400])
          tail -= 1
          lasers.remove(zap)
    
    for zap in laserremove:
      try:
        lasers.remove(zap)
      except:
        pass #Horrible code, don't cut off my balls

    for zap in backfire:
      zap[0] -= 4
      if zap[0] <= 240 and dead == 0 and dying == 0 and catmode in ("default", "laser"):
        dying = True
      elif zap[0] <= 240 and dead == 0 and dying == 0:
        backfire.remove(zap)
        dying = True
      for demon in attacklist:
        if zap[0] > (demon[0] - 20) and zap[0] < (demon[0] + 160):
          attacklist.remove(demon)
          score_dog += 2
          backfire.remove(zap)
          lastsplode += 1
          splodes[lastsplode] = [zap[0] - 100, zap[1] - 32]
          splodesprites[lastsplode] = 1
      for demon in downlist:
        if zap[0] > (demon[0] - 20) and zap[0] < (demon[0] + 160) and demon[1] > 320:
          downlist.remove(demon)
          score_dog += 2
          backfire.remove(zap)
          lastsplode += 1
          splodes[lastsplode] = [zap[0] - 100, zap[1] - 32]
          splodesprites[lastsplode] = 1
        
    for item in splodes:
      if frames % 7 == 0:
        if splodesprites[item] != 4:
          splodesprites[item] += 1
        else:
          splodes[item] = False
          splodesprites[item] = False
      
    dogsprite = ddogsprites[ndogsprite]
    fishsprite = dfishsprites[nfishsprite]
    
    if frames % spawninterval == 0: #Check if we be goinna spawn some shizzle, ma nizzle
      if dead == 0:
        if mode == "softmode":
          spawn([960, 408])
        elif mode == "hardmode":
          hardspawn([960,408])
        if bossstate == "stationary" and mode == "softboss":
          makedemonfish([960, 308])
        elif bossstate == "flee" and mode == "softboss":
          bossspawn2([960, random.randint(380, 399)])

    if int(difficulty) != 0 and bossstate in (0, "flee"):
      if frames % int(difficulty) == 0: #Increase difficulty
        fishspeed = fishspeed * 1.1#, dogspeed * 1.2
        spawninterval = spawninterval + 100
        difficulty = difficulty * 0.8
    
    
    for dog in doglist:
      if dog[0] <= 240 and dead == 0 and dying == 0:
        dying = True
      if dog[0] <= -136:
        doglist.remove(dog)
      dog[0] -= fishspeed

    
    for fish in fishlist:
      if fish[0] <= 240 and fish[0] >= 230:
        if catmode == "default" and mode == "softboss":
          fish[0] -= 1.9
        elif catmode == "default":
          fish[0] -= fishspeed
        elif catmode == "munch" and not dead: #Check if the cat is actually even trying
          fishlist.remove(fish)
          score_fish += 1
          hunger += 1
          effect.queue(ingested)
          eatstate = 5
        elif dead:
          fish[0] -= fishspeed
      elif fish[0] <= -136:
        fishlist.remove(fish)
      elif mode == "softboss":
        fish[0] -= 1.9
      else:
        fish[0] -= fishspeed
    
    for fishie in fallings:
      if fishie[0] <= -136:
        fallings.remove(fishie)
      else:
        fishie[0] -= dogspeed
    
    for demon in demonlist:
      if demon[0] > 350:
        demon[0] -= fishspeed
      else:
        if random.randint(1,5) == 1:
          downlist.append(demon)
          demonlist.remove(demon)
      if frames % interval == 0:
        if demon[1] == 308:
          demon[1] -= 2
        else:
          demon[1] += 2
    
    for demon in downlist:
      if demon[1] < 400:
        demon[1] += 1
      else:
        downlist.remove(demon)
        attacklist.append(demon)
    
    for demon in attacklist:
      if demon[0] <= 240 and dead == 0 and dying == 0:
        dying = True
      else:
        demon[0] -= 1.1
    
    for cray in craylist:
      if cray[0] <= 240 and cray[0] >= 230:
        if cray[0] <= 240 and dead == 0 and dying == 0:
          dying = True
      cray[0] -= 2.5
    
    for cray in cooklist:
      if cray[0] <= 240 and cray[0] >= 230:
        if catmode == "default" or catmode == "laser":
          if cray[0] <= 240 and dead == 0 and dying == 0:
            dying = True
            cray[0] -= 2.2
          elif dying or dead:
            cray[0] -= 2.2
        if catmode == "munch" and not dead: #Eat the cooked crayfish
          cooklist.remove(cray)
          score_fish += 1
          effect.queue(ingested)
          eatstate = 5
      else:
        cray[0] -= 2.2
    
    for item in groundlocs:
      if not dead and not dying and bossstate != "shield1" and bossstate != "shield2" and bossstate != "shield3" and bossstate != "stationary":
        item[0] -= dogspeed
        if item[0] <= -136:
          groundlocs.remove(item)

    for item in grasslocs:
      if not dead and not dying and bossstate != "shield1" and bossstate != "shield2" and bossstate != "shield3" and bossstate != "stationary":
        item[0] -= dogspeed
        if item[0] <= -136:
          grasslocs.remove(item)

    
    if frames % int(difvar) == 0:
      if not dead and not dying and bossstate != "shield1" and bossstate != "shield2" and bossstate != "shield3" and bossstate != "stationary":
        groundlocs.append([952, 544])
        grasslocs.append([952,424])


    if dying != 0: #Check if we are dying. Good life advice right there as well
      catsprite = ddyingcat[dying]
      cathead = False
      if dying <= 3:
        dying += 1
      else:
        effect.queue(inanimate)
        dying = 0
        dead = 1
    
    if dead != 0:
      cathead = False
      deadtext = True
      score = (2 * scoreframes) + (20 * score_fish) + (50 * score_dog) + score_boss - (10 * mscore_fish) #Calculate how shitty of a job the player has done
      scoretext = tinytext.render("SCORE: " + str(score), 1, (255,255,255))
      catsprite = ddeadcat[dead]
      if frames % interval == 0:
        if dead == 4:
          dead = 0
        dead += 1
    
    if int(difficulty) <= 1400:
      if mode == "softmode" and not dead and not dying:
        bossstate = "spawning"
    
    if bossstate == "spawning":
      challenger = True
      try:
        bosscoords
      except:
        bosscoords = [953,288]
      if bosscoords[0] > 670:
        bosscoords[0] -= dogspeed
      else:
        mode = "softboss"
        bossstate = "shield1"
    elif bossstate == "shield1":
      if frames % 21 == 0:
        shield1s = True
        bossstate = "shield2"
    elif bossstate == "shield2":
      if frames % 21 == 0:
        shield2s = True
        bossstate = "shield3"
    elif bossstate == "shield3":
      if frames % 21 == 0:
        shield3s = True
        bossstate = "stationary"
        fishspeed = 1.8
        challenger = False
    elif bossstate == "flee":
      if tail == 0: #If the boss is defeated
        if not won:
          won = True
          effect2.queue(implosion)
          explosions.append([0,0])
          hardmode = "unlocked"
          hunger = 1000000000000000000000000
    
    catheadloc = catheadlocs[ncatsprite]
    
    
    #music
    if music: #Check if the mixer is running
      if pygame.mixer.get_busy() == False: #If nothing is playing yet, we will play initiated
        music.play(initiated)
        music.queue(inane)
      elif music.get_queue() == None:
        if dead or dying:
          if first == False:
            first = True
            #pygame.mixer.quit() #Stop the mixer after playing the dead bit
        elif mode == "softmode":
          music.queue(inane)
        elif mode == "softboss" or mode == "hardboss":
          music.queue(indurated)
        elif mode == "hardmode":
          music.queue(inverse)

    
    #/music
    
    #/LOGIC
    #DRAW
    window.fill((100,170,220))
    
    for thing in groundlocs:
      window.blit(ground, thing) #Draw the ground
    
    for item in grasslocs:
      window.blit(grass, item) #Draw the grass
    
    for fish in fishlist:
      window.blit(fishsprite, fish) #Draw the fish
      
    window.blit(catsprite, (136,408)) #Draw the body of the cat
    if cathead:
      window.blit(cathead, catheadloc) #Then draw the head
    
    for fishie in fallings:
      window.blit(deadfish1, fishie) #Draw dead fish

    for dog in doglist:
      window.blit(dogsprite, dog) #Draw dogs
    
    if bossstate == "flee": #If we're in phase two of the bossfight
      window.blit(dfleeboss[tail], bosscoords)
      window.blit(dlegs[legtimer], (bosscoords[0]+96, bosscoords[1]+104))
      window.blit(dlegs[legtimer2], (bosscoords[0]+162, bosscoords[1]+104))
    
    elif bossstate: #If we're in the stationary bossfight part (including building up shield)
      window.blit(standing, bosscoords)
    
    for demon in demonlist:
      window.blit(demonfish1, demon)
    
    for demon in attacklist:
      window.blit(demonfish2, demon)
    
    for demon in downlist:
      window.blit(demonfish3, demon)
    
    for cray in craylist:
      window.blit(dsmallcray[ncatsprite], cray)
    
    for cray in cooklist:
      window.blit(dgcray[ncatsprite], cray)
    
    if shield1s:
      window.blit(shield1, (630, 272))
    
    if shield2s:
      window.blit(shield2, (570, 272))
    
    if shield3s:
      window.blit(shield3, (482, 272))

    for zap in lasers: #Draw lasers
      window.blit(laser, zap)

    for zap in backfire: #Draw lasers
      window.blit(laser, zap)

    for item in splodes:
      if splodes[item] != False:
        window.blit(dlaser[splodesprites[item]],splodes[item])
    
    if not won:
      window.blit(hungertext, (2, 52))
    
    if deadtext:
      window.blit(gameover, (2,2))
      window.blit(scoretext, (200,52))
    
    if challenger:
      window.blit(bosstext, (2,2))
    
    if won:
      window.blit(wontext, (2,2))
      window.blit(gameover, (2,52))

    
    pygame.display.flip() #Actually dr-paw stuff. Very nyanportant
    
    #/DRAW


#Load sprites:
ssprites = spritesheet.spritesheet("sprites/sprites_default.png")

#cat:
cat1,cat2,cat3,cat4 = ssprites.image_at((296,8, 136,136)),ssprites.image_at((440,8, 136,136)),ssprites.image_at((584,8, 136,136)),ssprites.image_at((728,8, 136,136))
dcatsprites = {1: cat1, 2: cat2, 3: cat3, 4: cat4}

#heads:
chewcat1, chewcat2, chewcat3, chewcat4 = ssprites.image_at((440,744, 136,136)),ssprites.image_at((584,744, 136,136)),ssprites.image_at((728,744, 136,136)),ssprites.image_at((584,744, 136,136))
dchews = {1: chewcat1, 2: chewcat2, 3: chewcat3, 4: chewcat4}
opencat1, opencat2, opencat3, opencat4 = ssprites.image_at((152,488, 136,136)),ssprites.image_at((296,488, 136,136)),ssprites.image_at((440,488, 136,136)),ssprites.image_at((296,488, 136,136))
dopencat = {1: opencat1, 2: opencat2, 3: opencat3, 4: opencat4}
nomcat1, nomcat2 = ssprites.image_at((8,744, 136,136)),ssprites.image_at((152,744, 136,136))
spit1, spit2 = ssprites.image_at((872,744, 136,136)),ssprites.image_at((1016,744, 136,136))
detccat = {1: nomcat1, 2: nomcat2, 3: spit1,4: spit2}
dyingcat1, dyingcat2 = ssprites.image_at((8,984, 136,136)),ssprites.image_at((152,984, 136,136))
ddyingcat = {1: dyingcat1, 2: dyingcat1, 3: dyingcat2, 4:dyingcat2}
deadcat1, deadcat2, deadcat3, deadcat4 = ssprites.image_at((440,984, 136,136)),ssprites.image_at((584,984, 136,136)),ssprites.image_at((728,984, 136,136)),ssprites.image_at((872,984, 136,136))
ddeadcat = {1:deadcat1,2:deadcat2,3:deadcat3,4:deadcat4}
lasercat1, lasercat2, lasercat3, lasercat4, lasercat5 = ssprites.image_at((8,248, 136,136)),ssprites.image_at((152,248, 136,136)),ssprites.image_at((296,248, 136,136)),ssprites.image_at((440,248, 136,136)),ssprites.image_at((584,248, 136,136))
dlasercat = {1:lasercat1,2:lasercat2,3:lasercat3,4:lasercat4,5:lasercat5}

#fish:
fish1,fish2,fish3,fish4 = ssprites.image_at((792,248, 136,136)),ssprites.image_at((936,248, 136,136)),ssprites.image_at((1080,248, 136,136)),ssprites.image_at((1224,248, 136,136))
deadfish1, deadfish2 = ssprites.image_at((824,488, 136,136)),ssprites.image_at((1112,488, 136,136))
dfishsprites = {1: fish1, 2: fish2, 3: fish3, 4: fish4, 99: deadfish1, 991: deadfish2}

#dog:
dog1,dog2,dog3,dog4, dog5, dog6 = ssprites.image_at((136,1232, 136,136)),ssprites.image_at((280,1232, 136,136)),ssprites.image_at((424,1232, 136,136)),ssprites.image_at((568,1232, 136,136)),ssprites.image_at((712,1232, 136,136)),ssprites.image_at((856,1232, 136,136))
ddogsprites = {1: dog1, 2: dog2, 3: dog3, 4: dog4, 5: dog5, 6: dog6}

#demonfish:
demonfish1,demonfish2,demonfish3 = ssprites.image_at((1376,40, 208,144)),ssprites.image_at((1592,40, 240,144)),ssprites.image_at((1872,48, 240,160))
ddemonfish = {1:demonfish1,2:demonfish2,3:demonfish3}

#ground:
ground, grass = ssprites.image_at((1576,248, 136,136)),ssprites.image_at((1720,248, 136,136))
dterrain = {2: grass}

#boss:
standing = ssprites.image_at((1320,1040, 944,256))
dboss = {1:standing}
fleeboss6,fleeboss5,fleeboss4,fleeboss3,fleeboss2,fleeboss1,fleeboss0 = ssprites.image_at((3200,576, 944,256)),ssprites.image_at((1320,1304, 944,256)),ssprites.image_at((2272,1040, 944,256)),ssprites.image_at((2272,1304, 944,256)),ssprites.image_at((3224,1040, 944,256)),ssprites.image_at((3224,1304, 944,256)),ssprites.image_at((3224,1568, 944,256))
dfleeboss = {6:fleeboss6,5:fleeboss5,4:fleeboss4,3:fleeboss3,2:fleeboss2,1:fleeboss1,0:fleeboss0}
legs1,legs2,legs3 = ssprites.image_at((2176,1968, 272,176)),ssprites.image_at((3064,1968, 272,176)),ssprites.image_at((4152,1968, 272,159))
dlegs = {1:legs1,2:legs2,3:legs3}

shield1, shield2, shield3 = ssprites.image_at((1244,1032, 60,272)),ssprites.image_at((1176,1032, 60,272)),ssprites.image_at((1088,1032, 88,272))
dshields = {1:shield1,2:shield2,3:shield3}

smallcray1,smallcray2,smallcray3,smallcray4 = ssprites.image_at((2144,408, 128,136)),ssprites.image_at((2280,408, 136,136)),ssprites.image_at((2424,408, 128,136)),ssprites.image_at((2560,408, 128,136))
dsmallcray = {1:smallcray1,2:smallcray2,3:smallcray3,4:smallcray4}
gcray1,gcray2,gcray3,gcray4 = ssprites.image_at((2144,552, 128,136)),ssprites.image_at((2280,552, 136,136)),ssprites.image_at((2424,552, 128,136)),ssprites.image_at((2560,552, 128,136))
dgcray = {1:gcray1,2:gcray2,3:gcray3,4:gcray4}

#etc:
laser, hit4, hit3, hit2, hit1 = ssprites.image_at((1592,440, 40,16)),ssprites.image_at((1728,408, 80,80)),ssprites.image_at((1816,408, 80,80)),ssprites.image_at((1880,408, 72,80)),ssprites.image_at((1928,408, 80,80))
dlaser = {1:hit1,2:hit2,3:hit3,4:hit4}

explosion1,explosion2 = ssprites.image_at((685,1376, 196,192)), ssprites.image_at((928,1376, 272,272))

allsprites = [dcatsprites, dfishsprites, ddogsprites, dchews, dopencat, detccat, ddyingcat, ddeadcat, dterrain, dlasercat, dlaser, dboss, dshields, dfleeboss, ddemonfish, dsmallcray, dgcray, dlegs]


#240

for spritelist in allsprites:
  for image in spritelist:
    color = spritelist[image].get_at((0,0))
    spritelist[image].set_colorkey(color)

#AI (arti-fish-ial intelligence):

def spawn(coords):
  if random.randint(1, 3) == 1:
    makefish(coords)
  else:
    makedog(coords)

def hardspawn(coords):
  vari = random.randint(1, 5)
  if vari == 1:
    makefish(coords)
  elif vari in (2,3,4,5):
    makedog(coords)

def bossspawn1(coords):
  vari = random.randint(1, 2)
  if vari == 1:
    makefish(coords)
  else:
    makedemonfish(coords)

def bossspawn2(coords):
  craylist.append(coords)
  effect.queue(inspace)
  makefish([960, 408])
  
  
def makefish(coords):
  fishlist.append([coords[0], coords[1] + 16])

def makedog(coords):
  doglist.append(coords)

def makedemonfish(coords):
  demonlist.append(coords)


if __name__ == "__main__":
  menu("softmode")
